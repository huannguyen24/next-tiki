// need to define headersObject later
const setAuthHeaders = (headersObject: any) => {
    let headers = { ...headersObject };
    if (localStorage.jwtToken) {
        headers = {
            ...headers,
            Authorization: `Bearer ${localStorage.jwtToken}`
        };
    }
    return headers;
};
export default setAuthHeaders;
