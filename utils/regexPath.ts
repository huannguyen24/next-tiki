const regexPath = (text: string) => {
    return text.replace(/\\/g, '/');
}

export default regexPath;