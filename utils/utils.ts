// need to define all params have type any
export const textTruncate = (text: any, length: any, ending: any) => {
    if (length == null) {
        length = 100;
    }
    if (ending == null) {
        ending = '...';
    }
    if (text.length > length) {
        return text.substring(0, length - ending.length) + ending;
    } else {
        return text;
    }
};

export const match = (term: any, array: any, key: any) => {
    const reg = new RegExp(term.split('').join('.*'), 'i');
    return array.filter((item: any) => item[key] && item[key].match(reg));
};
