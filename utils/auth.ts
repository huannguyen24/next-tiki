export const isBrowser = () => typeof window !== 'undefined';
export const getUser = () =>
    isBrowser() && window.localStorage.getItem('user')
        ? JSON.parse((window.localStorage.getItem('user') as string))
        : {};

export const setUser = (user: string) =>
    window.localStorage.setItem('user', JSON.stringify(user));
export const removeUser = () => window.localStorage.removeItem('user');
export const removeToken = () => window.localStorage.removeItem('jwtToken');

export const isLoggedIn = () => {
    const user = getUser();
    return !!user.username;
};
