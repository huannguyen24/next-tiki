"use client";

import React, { createContext, useContext } from "react";

// Create a context object
const PublicContext = createContext({});

// Create a custom hook to access the context
export const usePublicContext = () => {
  return useContext(PublicContext);
};

// CinemaProvider component to wrap your application
export const PublicProvider = ({ children }: { children: React.ReactNode }) => {
  // Define your state variables
  // const [genres, setGenres] = useState<IGenres>({ genres: [] });

  // Define functions to update the state

  // Define the context value
  const contextValue = {
    // genres,
    // setGenres,
  };

  // Return the provider with the context value
  return (
    <PublicContext.Provider value={contextValue}>
      {children}
    </PublicContext.Provider>
  );
};
