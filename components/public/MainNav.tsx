import DarkModeToggle from "../DarkModeToggle";
import { DropdownLanguagesMenu } from "../LangDropDown";
import SearchInput from "./SearchInput";
import { Separator } from "@/components/ui/separator";
import UserController from "./UserController";
import ShoppingCarts from "./ShoppingCarts";
import { searchCategories } from "@/mock/categories.constants";
import Link from "next/link";
import HomePageBtn from "./HomepageBtn";
import { ISearchCategories } from "@/typing/public/typing";
import LocationPointer from "./LocationPointer";

const MainNav = () => {
  return (
    <div className="flex flex-col flex-1">
      <div className="flex flex-row flex-1 gap-5 justify-between mb-[0.8rem]">
        {/* DARK MODE THEME, LANGS AND SEARCH ...*/}
        <div className="w-full">
          <SearchInput />
        </div>
        <div className="flex space-x-2 items-center">
          <HomePageBtn />
          <DropdownLanguagesMenu />
          <DarkModeToggle />
          <UserController />
        </div>
        <Separator orientation="vertical" />
        <ShoppingCarts />
      </div>
      <div className="flex flex-row flex-1 gap-5 justify-between">
        <div className="flex-wrap flex gap-5">
          {searchCategories.map(({ name, id, link }: ISearchCategories) => (
            <Link key={id} href={link}>
              <span className="cursor-pointer text-gray-400 text-sm">
                {name}
              </span>
            </Link>
          ))}
        </div>
        <LocationPointer />
      </div>
    </div>
  );
};

export default MainNav;
