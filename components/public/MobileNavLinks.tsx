import Link from "next/link";
import { Button } from "../ui/button";

const MobileNavLinks = () => {
  return (
    <>
      <Link
        href="/user-profile"
        className="flex bg-whiote items-center font-bold hover:text-blue-500 "
      >
        User Profile
      </Link>
      <Button
        className="flex-1 items-center px-3 font-bold hover:bg-gray-500"
        onClick={() => {}}
      >
        Log Out
      </Button>
    </>
  );
};

export default MobileNavLinks;
