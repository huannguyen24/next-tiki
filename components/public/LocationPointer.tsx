"use client";
import { MapPin } from "lucide-react";

import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Label } from "@/components/ui/label";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { Separator } from "../ui/separator";

const LocationPointer = () => {
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="ghost">
          <MapPin />
          Giao đến: 
          <span className="underline">Q. Hoàn Kiếm, P. Hàng Trống, Hà Nội</span>
        </Button>
      </DialogTrigger>
      <DialogContent className="min-w-[600px] p-8">
        <DialogHeader className="flex flex-col items-center gap-6">
          <DialogTitle className="m-auto text-xl">
            Địa chỉ giao hàng
          </DialogTitle>
          <Separator />
          <DialogDescription>
            Hãy chọn địa chỉ nhận hàng để được dự báo thời gian giao hàng cùng
            phí đóng gói, vận chuyển một cách chính xác nhất.
          </DialogDescription>
          <Button
            className="bg-yellow-400 hover:bg-yellow-300 text-black font-normal"
            onClick={() => {}}
          >
            Đăng nhập để chọn địa chỉ giao hàng
          </Button>
        </DialogHeader>
        <div className="relative flex justify-center">
          <span className="text-sm">hoặc</span>
        </div>
        <RadioGroup defaultValue="default">
          <div className="flex items-center space-x-4 mb-4">
            <RadioGroupItem value="default" id="r1" />
            <Label htmlFor="r1">Phường Hàng Trống, Quận Hoàn Kiếm, Hà Nội</Label>
          </div>
          <div className="flex items-center space-x-4 mb-4">
            <RadioGroupItem value="comfortable" id="r2" />
            <Label htmlFor="r2">Chọn khu vực giao hàng khác</Label>
          </div>
        </RadioGroup>
        <DialogFooter className="sm:justify-center flex flex-col items-center">
          <DialogClose asChild>
            <Button
              type="button"
              variant="destructive"
              className="uppercase text-white px-8"
            >
              Giao đến địa chỉ này
            </Button>
          </DialogClose>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default LocationPointer;
