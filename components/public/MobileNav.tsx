'use client'

import React from "react";
import {
  Sheet,
  SheetContent,
  SheetDescription,
  SheetTitle,
  SheetTrigger,
} from "../ui/sheet";
import { Menu } from "lucide-react";
import { Separator } from "../ui/separator";
import { Button } from "../ui/button";

const MobileNav = () => {
  return (
    <Sheet>
      <SheetTrigger >
        <Menu className="text-blue-500 "  />
      </SheetTrigger>
      <SheetContent className="space-y-3 ">
        <SheetTitle>
          {/* {isAuthenticated ? <span className='flex items-center font-bold gap-2'>
                        <CircleUserRound className='text-orange-500' />
                        {user?.email}
                    </span> : */}
          <span>Welcome to Cinema Plus!</span>
          {/* } */}
        </SheetTitle>
        <Separator />
        <SheetDescription className="flex flex-col gap-4">
          {/* {isAuthenticated ? <MobileNavLinks /> :  */}
          <Button
            className=" text-blue-500 flex-1 font-bold"
            onClick={() => {}}
          >
            Log in
          </Button>
          {/* } */}
        </SheetDescription>
      </SheetContent>
    </Sheet>
  );
};

export default MobileNav;
