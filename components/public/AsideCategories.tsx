import { ScrollArea } from "@/components/ui/scroll-area";
import { Bird } from "lucide-react";
import { asideCategories } from "@/mock/categories.constants";
import { Button } from "../ui/button";

const AsideCategories = () => {
  return (
    <aside>
      <ScrollArea className="w-full h-lvh rounded-md border">
        <div className="p-4">
          <h4 className="mb-4 text-sm font-medium leading-none">Danh mục</h4>
          <div className="flex flex-col items-start">
            {asideCategories.data.map(
              ({ id, name }: { id: string; name: string }) => (
                <Button key={id} variant="ghost" className="flex flex-row gap-1">
                  <Bird className="h-[1.2rem] w-[1.2rem] rotate-0 scale-100" />
                  {name}
                </Button>
              )
            )}
          </div>
        </div>
      </ScrollArea>
    </aside>
  );
};

export default AsideCategories;
