"use client";
import { Button } from "../ui/button";
import { useRouter } from "next/navigation";
const HomePageBtn = () => {
  const router = useRouter();

  return (
    <Button variant="ghost" onClick={() => router.push("/")}>
      Trang chủ
    </Button>
  );
};

export default HomePageBtn;
