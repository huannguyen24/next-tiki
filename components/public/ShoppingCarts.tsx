import { ShoppingCart } from "lucide-react";
import { Button } from "../ui/button";

const ShoppingCarts = () => {
  return (
    <div className="indicator">
      <span className="indicator-item badge badge-secondary badge-sm">9</span>
      <Button variant="ghost" size="icon">
        <ShoppingCart className="h-[1.2rem] w-[1.2rem] rotate-0 scale-100" />
      </Button>
    </div>
  );
};

export default ShoppingCarts;
