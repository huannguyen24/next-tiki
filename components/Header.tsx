import Image from "next/image";
import Link from "next/link";
import React from "react";
import logo from "@/public/Nextify.svg";
import { ShoppingBag } from "lucide-react";
import MainNav from "@/components/public/MainNav";
import MobileNav from "@/components/public/MobileNav";
const PublicHeader = () => {
  return (
    <header
      className={`container gap-5 flex flex-row items-center justify-between bg-white bg-gradient-to-t dark:from-gray-200/0 dark:via-gray-900/25 dark:to-gray-900 w-full fixed top-0 left-0 right-0 py-[0.8rem] px-[20px] flex-wrap z-50 transition-all duration-300 ease-in-out border-b-2 border-b-gray-200`}
    >
      <Link
        className="flex flex-col justify-center items-center gap-1 relative z-10 py-1"
        href="/"
      >
        <ShoppingBag className="size-[1.5rem] scale-100 transition-all" />
        <Image
          src={logo}
          alt="NEXT TIKI for you"
          sizes="100vw"
          priority
          className="w-full h-auto max-h-[2.4rem] dark:invert cursor-pointer invert-0"
        />
      </Link>

      <div className="md:hidden">
        <MobileNav />
      </div>
      <div className="hidden md:flex flex-1 flex-row ">
        <MainNav />
      </div>
    </header>
  );
};

export default PublicHeader;
