export interface ISearchCategories {
  name: string;
  id: string;
  link: string;
}
